FROM golang:1.17-alpine AS build
WORKDIR /src
COPY main.go go.mod /src/
RUN CGO_ENABLED=0 go build -o helloworld ./main.go

FROM scratch
USER 1000
ENTRYPOINT ["/helloworld"]
COPY --from=build /src/helloworld /helloworld
